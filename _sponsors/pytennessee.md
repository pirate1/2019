---
name: PyTennessee
tier: community
site_url: https://2020.pytennessee.org/
logo: pytennessee.png
twitter: PyTennessee
---
PyTennessee is a yearly regional Python conference held in the late Winter/early Spring in
Nashville, TN. PyTennessee 2020 will be in early March and will be our 7th year, and just like every
year before it, is sure to be our best yet! With 3 talk tracks, a tutorial track, and a killer
hallway track, there's a bit of something for everyone at PyTennessee 2020, and if Python
conferences are your thing, we'd love to see you in March!
