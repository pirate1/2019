---
name: PyCarolinas
tier: community
site_url: http://pycarolinas.org/
logo: pycarolinas.png
twitter: PyCarolinas
---
PyCarolinas is coming back, after 8 long years! Come join us on June 20-21, 2020 at the Red Hat
Annex in downtown Raleigh, NC for PyCarolinas 2020. We plan to have two full days of exciting talks
about all things Python: language tricks, web development, testing, data science, cool projects, and
more. Sign up for the newsletter at [pycarolinas.org](http://pycarolinas.org) to keep in touch. The
CFP should be available starting January 1, 2020!

Dates: June 20-21, 2020

Location: Red Hat Annex, 190 E Davie St, Raleigh, NC 27601
