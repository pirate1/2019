---
name: Two Sigma
tier: silver
site_url: https://www.twosigma.com/
logo: twosigma.jpg
twitter: twosigma
---
Two Sigma is a different kind of investment manager. Since 2001, we have used data science and
technology to derive insights that forecast the future and uncover value in markets worldwide.

Our team of scientists, technologists, and academics looks beyond the traditional to understand the
bigger picture and develop creative solutions to some of the world’s most challenging economic
problems. Our work spans across markets and industries, from insurance and securities to private
investments and new ventures.  
