---
duration: 40
presentation_url: https://github.com/bradmiro/talks/blob/master/PyGotham2019/python_distributed_ml.pdf
room: Madison
slot: 2019-10-05 11:15:00-04:00
speakers:
- Brad Miro
title: Distributed Machine Learning with Python
type: talk
video_url: null
---

As the amount of data continues to grow, the need for distributed machine
learning continues to grows with it. We'll discuss techniques and methods
for distributing machine learning training such as data parallelism and
model parallelism. We'll then discuss how to build scalable machine learning
systems and show two examples of this using Python: one with Apache Spark
MLlib, one with TensorFlow.
