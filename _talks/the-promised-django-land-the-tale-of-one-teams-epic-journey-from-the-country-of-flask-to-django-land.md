---
duration: 25
presentation_url: null
room: Madison
slot: 2019-10-04 10:45:00-04:00
speakers:
- Nicole Zuckerman
title: "The promised Django Land; the tale of one team\u2019s epic journey from the\
  \ country of Flask to Django Land"
type: talk
video_url: null
---

Your product doesn't stop while you take the time to do a massive rewrite in
order to change your language or framework.  It seems everyone has an
opinion on what the 'best' tools are, and views changing tools as a red
flag.  Many organizations do change frameworks or languages mid-stream, to
greater or lesser success; in this talk, you'll get a real-life fairy tale
of one company's move from Flask to Django, why each was the best choice at
the time, how they got team buy-in, what technical decisions made things
easier or harder, and how they switched without stopping all product work.
