---
duration: 40
presentation_url: https://speakerdeck.com/flipperpa/pygotham-absolutely-awesome-automated-apis
room: PennTop North
slot: 2019-10-04 11:15:00-04:00
speakers:
- Timothy Allen
title: Absolutely Awesome Automated APIs
type: talk
video_url: null
---

When you have 60,000 tables and views across hundreds of schemata sitting on
top of a 3 petabyte storage footprint, automation and access privileges are
key if you want to expose these data through a RESTful API. Our University's
research service leverages Python, Django, Django REST Framework, and
PostgreSQL to accomplish this. We are continuing to open source the tools we
have built to make this possible!

Databases stay relevant by continuing to reinvent themselves to serve new
technologies further up the stack. The latest buzzwords further up that
stack are APIs and microservices, so prevalent that it is hard to see a tech
advertisement that doesn’t mention them. While related to “the cloud” and
“big data”, whatever the heck those terms actually mean, APIs and
microservices have a slightly less annoying marketing schtick and more
concrete relations to relational databases.

But what do these relations look like in practice? In this talk, the speaker
will show how his team at the University has evolved from providing
financial data exclusively in SAS data formats to a robust backend powered
by PostgreSQL, which allows financial research to happen in many ecosystems:
still available in SAS, but also R, Python, Perl, Matlab, Julia, and more.
The speaker will present a case study of using Django REST Framework to
build an “API through introspection.” This case study will show how Django
web site and RESTful web service were built by introspecting financial data
stored in a PostgreSQL database cluster.

The models for the ORM, serializers for the RESTful API, views for
presenting the data to a user, filters for refining queries, URL routing,
web browsable interface, user token authorization, and permissions, are all
created by introspecting the PostgreSQL database information schema, all
with Python. These components have now been open-sourced, including the
endpoint spreadsheet exporter and the generic automated API builder.
