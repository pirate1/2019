---
duration: 25
presentation_url: https://docs.google.com/presentation/d/1_ST-4SqamrvANCCS46I1jrDlQekRlbcfVPFTZnhY47w/edit#slide=id.g635adc05c1_1_79
room: Madison
slot: 2019-10-05 10:15:00-04:00
speakers:
- Nathan Cheever
title: '1000x faster data manipulation: vectorizing with Pandas and Numpy'
type: talk
video_url: null
---

The data transformation code you're writing is correct, but potentially
1000x slower than it needs to be! In this talk, we will go over multiple
ways to enhance a data transformation workflow with Pandas and Numpy by
showing how to replace slower, perhaps more familiar, ways of operating on
Pandas data frames with faster-vectorized solutions to common use cases
like:

* if-else logic in applied row-wise functions
* dictionary lookups with conditional logic
* Date comparisons and calculations
* Regex and string column manipulation
* and others! ...


without needing a beefier computer, writing Cython, or other libraries
outside the Pandas ecosystem.
