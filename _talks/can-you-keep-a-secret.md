---
duration: 25
presentation_url: https://noti.st/aaronbassett/X8TLo6/can-you-keep-a-secret
room: PennTop North
slot: 2019-10-04 10:45:00-04:00
speakers:
- Aaron Bassett
title: Can you keep a secret?
type: talk
video_url: null
---

We’ve all been guilty of hard-coding secrets at some point. It’s just a
quick hack, and you’ll definitely go back and tidy it up later. But then you
forget, and it’s all too easy to `git push` your API keys to GitHub.

This easy to make mistake could end up [costing you thousands of
dollars](https://dev.to/juanmanuelramallo/i-was-billed-for-14k-usd-on-
amazon-web-services-17fn), and with the [median time to discovery for a
secret key leaked to GitHub being 20 seconds](https://www.ndss-
symposium.org/wp-content/uploads/2019/02/ndss2019_04B-3_Meli_paper.pdf) you
could end up compromised before you have a chance to correct your error.

In this talk, we’ll look at techniques that you can use personally and
within your development teams to properly store, share, and manage your
secrets, as easily as possible without disrupting your workflow.
