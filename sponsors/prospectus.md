---
title: Sponsorship Prospectus
---

## So you want to sponsor {{ site.data.event.name }}...

You're awesome!

{{ site.data.event.name }} is New York City's largest gathering of Python
developers. Brought to you by a dedicated group of community organizers, the
conference draws a diverse crowd with backgrounds in many different industries.

## Quick Facts

- The conference has sold out every year since 2014, and we expect to do so
again with over 500 attendees.

- The schedule contains two full days of talks in three tracks, on-site lunch
and break times, and an evening social event. Talks will cover topics including
core Python, data science, web development, and more. For an idea of what to
expect, take a look previous years' schedules:
[2018](https://2018.pygotham.org/talks/schedule/),
[2017](https://2017.pygotham.org/talks/schedule/),
[2016](https://2016.pygotham.org/talks/schedule/),
[2015](https://2015.pygotham.org/talks/schedule/),
[2014](https://2014.pygotham.org/talks/schedule.html).

- PyGotham strives to be accessible to everyone. Over the years, we've awarded
scholarship tickets to hundreds of attendees. In 2017, we began offering travel
and lodging grants, as well. Each year, our financial aid requests grow, and we
need generous sponsors to continue this program.

In order to keep the registration costs as low as possible, we need your help.
Several different levels of sponsorship are available. If you have any
questions, you can contact us directly.

Once you're ready, go ahead and send an email to
[sponsors@pygotham.org](mailto:sponsors@pygotham.org).

## Sponsorship Levels

| Benefit                            | Platinum  | Gold       | Silver   | Startup  |
|                                    | $15,000   | $7,500     | $2,500   | $1,000   |
| ---------------------------------- | :-------: | :--------: | :------: | :------: |
| Limited to                         | 1         | 8          | ∞        | ∞        |
| Sponsor talk (subject to approval) | ✅        |            |          |          |
| Logo on video bumper               | ✅        |            |          |          |
| Booth in expo hall                 | Large     | Small      |          |          |
| Logo in conference media           | ✅        | ✅         | ✅       | ✅       |
| Scholarship tickets                | 5         | 3          | 1        | 1        |
| Tickets                            | 10        | 6          | 2        | 2        |
| Discount on additional tickets     | 20%       | 20%        | 20%      | 20%      |
{: .table.table-striped.table-bordered }

## Other Forms of Sponsorship

| Benefit                                           | Social Event  | Lanyards  | Financial Aid |
|                                                   | $5,000*       | $3,000**  |               |
| ------------------------------------------------- | :-----------: | :-------: | :-----------: |
| Limited to                                        | 3             | 1         | ∞             |
| Support the next generation of Python programmers |               |           | ✅            |
| Logo in conference media                          | ✅            | ✅        | ✅            |
| Conference Tickets                                | 4             |           |               |
{: .table.table-striped.table-bordered }

\* Discounted social event sponsorship pricing is available for sponsors of our
main tiers.

\*\* Lanyard sponsorship is only available as an add-on to other sponsorship
tiers. The cost of the custom printed lanyards is included in this price. The
lanyard design will include the sponsor's logo and the {{ site.data.event.name }}
logo.

{{ site.data.event.name }} will offer additional forms of sponsorship on top of
and in addition to the tiers included above. Details will be added as they are
finalized.

## FAQ

### Do I qualify for the startup tier?
We consider any company under three years of age with 50 or fewer employees to be
a startup.

### Why does a sponsored talk need to be approved?
All talks go through the talk review process to ensure that PyGotham remains a
high quality conference. Sponsored talks should be the same kind of talks that
you would submit through our normal call for proposals. Sales pitches and talks
that are not relevant to the Python community will be rejected.

### What are scholarship tickets?
The PyGotham financial aid program provides free tickets to attendees who would
not otherwise be able to attend. This program has helped hundreds of people
attend the conference in the past, and sponsorships make it possible. This year,
PyGotham is proud to be offering travel and lodging assistance, as well. (Want
to help with financial grants directly? Reach out!)
