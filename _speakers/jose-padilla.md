---
name: José Padilla
talks:
- "Python, Governments, and Contracts"
---

José Padilla is a pragmatic software engineer, hacker, and entrepreneur from Puerto Rico, currently living in Connecticut with his wife and miniature dachshund. He loves bringing people together through open source, meetups and events. Most recently he's organizing the Greater Hartford Python meetup, DevOpsDays Hartford, and the Code for Connecticut brigade. He currently works at Auth0, and on his side project, FilePreviews.io.