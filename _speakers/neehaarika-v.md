---
name: Neehaarika Velavarthy
permalink: /speakers/neehaarika-v/
talks:
- "Analyzing the Evolution of Irish Traditional Music using Python"
---
I work in Risk Management in roles that have spanned Payments, Fraud and
Prepaid Risk. My first foray into Python was three years ago and I have
since regretted not taking it up sooner. It has been something of a roller-
coaster ride and I have been blown away by the support in the form of Python
packages and the know-how on Stack Overflow.
<br />
Five years ago, out of
boredom, I bought a violin and taught myself a couple of Irish tunes and
have been unleashing them on unsuspecting listeners ever since. The warm and
welcoming community of Irish folk musicians, here in New York, is a big
reason I love this music and its musicians.
