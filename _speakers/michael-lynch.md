---
name: Michael Lynch
talks:
- "Why Good Developers Write Bad Tests"
---
Michael Lynch is a software developer and blogger. He has worked as a
software engineer at Microsoft and Google, and as a security engineer at NCC
Group. He is available for weddings, bar mitzvahs, and other events that are
likely to serve chocolate-covered strawberries.

You can read about his adventures in entrepreneurship and programming at
on his blog at [mtlynch.io](https://mtlynch.io) and on Twitter
[@deliberatecoder](https://twitter.com/deliberatecoder).
