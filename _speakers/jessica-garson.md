---
name: Jessica Garson
talks:
- "How I Solved my NYC Parking Problem with Python "
---
Jessica Garson is a Python programmer, educator, and artist. She currently
works at Twitter as a Developer Advocate. Previously, she was an adjunct
professor teaching Python at NYU and worked at ISL, Burson-Marstellar, The
National Education Association, ISSI Data, and Salsa Labs. Before working in
technology, Jessica worked on numerous political campaigns throughout the
country. She has run many meetups and conferences including DC’s Tech Lady
Hackathon in 2016 and 2017, and DC’s Hack&&Tell. In her spare time, she is
on a never-ending quest for the perfect vegan snack.
