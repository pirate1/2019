---
title: Talk schedule now available!
date: 2019-08-19 10:00:00 -0400
excerpt_separator: <!--more-->
---

The PyGotham 2019 [talk schedule](/talks/schedule/) is now available. It
features three tracks spread across October 4th and 5th.

<!--more-->

This year's program features 65 speakers giving 63 talks covering a wide
array of topics, from application design to data science to natural language
processing to web development. There will also be three keynote talks and
lightning talks.

Thanks to everyone who submitted a talk, voted on talks, or signed up for
the program committee.
